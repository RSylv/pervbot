// On importe notre Class 'Bot'
import BotPervert from "./Class/Bot.js"

// On déclare les variables
const container = document.getElementById('app-container')
const button = document.querySelector('.btn-question')

// Si l'utilisateur click sur le bouton 'Question ?'
button.addEventListener('click', async () => {

    // On récupère la valeur de l'input select 'Choices'
    const select = document.getElementById('cat')

    // On crée un nouveau Bot
    const Bot = await new BotPervert(select.value)
    
    // On vide le container
    container.textContent = ''

    // on crée la div 
    const div = document.createElement('div')

    // On y intègre les propriétés de notre Bot (new BotPervert())
    div.innerHTML = '<p class="question">' + Bot.message.question + '<span class="triangle"></span></p>'
    div.innerHTML += `<img src="${Bot.imageSrc}" alt=""><br>`
    div.innerHTML += '<p class="bot-info">' + Bot.character.name + ' ' + Bot.character.age + ' ans.</p>'
    div.innerHTML += `<button class="btn">Oui</button> <button class="btn">Non</button>`

    // On envoi dans le Dom/ l'Html
    container.append(div)

    // On prépare la réponse 
    waitForAnswer(Bot.message)
})


/**
 * Préparation de l'affichage de la réponse en fonction du choix de réponse 'Oui' ou 'Non'
 * @param {messages} msg 
 */
function waitForAnswer(msg) {

    // pour chacun des boutton 'OUI' ou 'NON'
    document.querySelectorAll('.btn').forEach((btn) => {
        btn.addEventListener('click', () => {

            // On déclare une réponse vide
            let answer = '';

            // On check si la réponse est Oui ou Non
            btn.textContent === 'Non' ? answer = msg.no : answer = msg.ok;

            // On cree une div et on y affiche la réponse
            const div = document.createElement('div')
            div.innerHTML = '<p class="question answer">' + btn.textContent + '</p>'
            div.innerHTML += '<p class="question">' + answer + '</p>'

            // On envoi dans le Dom/ l'Html
            container.append(div)

            // On éfface les boutons de réponse
            document.querySelectorAll('.btn').forEach((btn) => { btn.remove()})
        })
    })
}
