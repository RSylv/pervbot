const characters = [
    { name: 'Karolyn', age: 360 },
    { name: 'Pepito', age: 10 },
    { name: 'Andomie', age: 56 },
    { name: 'Sosoana', age: 780 },
    { name: 'Dunkan Mc Leod', age: 2800 },
    { name: 'Macron', age: 4 },
    { name: 'Pristophe', age: 210 },
    { name: 'Madelaine', age: 110 },
    { name: 'Alejo', age: 73 },
    { name: 'Sylavin', age: 83 },
]

export default characters;