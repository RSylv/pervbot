import characters from '../DATA/characters.js';
import messages from '../DATA/messages.js';

// https://robohash.org/sdgddfgdfdfgdssfsqf?set=set1

export default class BotPervert {

    /**
     * Constructeur de robot !
     * @param {string} choice 
     */
    constructor(choice = 'soft') {
        this.choice = choice
        this.character = this.getCharacter()
        this.message = this.getMessage()
        this.imageSrc = this.getImgSrc()

    }

    /**
     * Récupère un personnage aleatoirement dans le tableau characters.
     * @returns {characters} UN SEUL objet character
     */
    getCharacter() {
        return characters[this.getRandom(characters.length)]
    }

    /**
     * Récupère un message aleatoirement dans le tableau messages,
     * selon la categorie choisie par l'utilisateur     * 
     * @returns {messages} UN SEUL objet message
     */
    getMessage() {
        // On crée un tableau vide
        const msgs = [];

        // On analyse chaque élément du tableau/DATABASE 'messages'
        messages.map(msg => {

            // On insère dans le nouveau tableau, les messages ou la categorie (cat) 
            // est strictement égale au choix de l'utilisateur.
            if (msg.cat === this.choice) { msgs.push(msg) }
        })

        // On retourne un élément du tableau crée, selon un index aleatoire
        return msgs[this.getRandom(msgs.length)]
    }

    /**
     * Construit un lien vers l'api robohash, qui genere une image/avatar aléatoire de robot.
     * https://robohash.org/
     * @returns {string} src de l'image générée
     */
    getImgSrc() {
        const numb = this.getRandom(4, 1)
        const hash = this.getRandom(0, 0, true)
        return `https://robohash.org/${hash}?set=set${numb}`;
    }

    /**
     * 
     * @param {number} number Un nombre max (-1)
     * @param {number} base Un nombre de depart. defaut = **0**
     * @param {boolean} isText defaut **false**. Si **true** renvoie une chaine de 16 caractères aléatoire (1-9, a-z) 
     * @returns { string | number } Un nombre , ou une chaine base36, aleatoires
     */
    getRandom(number, base = 0, isText = false) {
        if (!isText) { return Math.floor(Math.random() * number) + base }
        else if (isText) {
            return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2)
        }
    }
}
